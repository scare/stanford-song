//
//  Photo.m
//  PhotoMania
//
//  Created by Daniel Hirschlein on 1/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Photo.h"
#import "Photographer.h"


@implementation Photo

@dynamic title;
@dynamic subtitle;
@dynamic imageurl;
@dynamic unique;
@dynamic whoTook;

@end
