//
//  FlickrPhotoTableViewController.h
//  ShutterBug
//
//  Created by Daniel Hirschlein on 12/29/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlickrPhotoTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *photos; // of Flickr photo dictionaries

@end
