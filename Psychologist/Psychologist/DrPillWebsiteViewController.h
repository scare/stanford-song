//
//  DrPillWebsiteViewController.h
//  Psychologist
//
//  Created by Daniel Hirschlein on 12/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DrPillWebsiteViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
