//
//  DrPillWebsiteViewController.m
//  Psychologist
//
//  Created by Daniel Hirschlein on 12/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "DrPillWebsiteViewController.h"

@implementation DrPillWebsiteViewController
@synthesize webView;


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.webView.scalesPageToFit = YES;
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.google.com"]]];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation   
{
    return YES;
}

@end
