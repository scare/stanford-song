//
//  PsychologistAppDelegate.h
//  Psychologist
//
//  Created by Daniel Hirschlein on 12/18/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PsychologistAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
