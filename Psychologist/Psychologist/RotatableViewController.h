//
//  RotatableViewController.h
//  Psychologist
//
//  Created by Daniel Hirschlein on 12/19/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RotatableViewController : UIViewController <UISplitViewControllerDelegate>

@end
