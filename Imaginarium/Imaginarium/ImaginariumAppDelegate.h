//
//  ImaginariumAppDelegate.h
//  Imaginarium
//
//  Created by Daniel Hirschlein on 12/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImaginariumAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
