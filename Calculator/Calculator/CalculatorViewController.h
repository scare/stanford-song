//
//  CalculatorViewController.h
//  Calculator
//
//  Created by Daniel Hirschlein on 12/6/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorViewController : UIViewController 
    @property (weak, nonatomic) IBOutlet UILabel *description;
    @property (weak, nonatomic) IBOutlet UILabel *display;
    @property (weak, nonatomic) IBOutlet UILabel *testVariables;

@end
