//
//  GraphViewController.m
//  Calculator
//
//  Created by Daniel Hirschlein on 12/19/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "GraphViewController.h"
#import "GraphView.h"
#import "CalculatorBrain.h"
#import "CalculatorProgramsTableViewController.h"

#define FAVORITES_KEY @"CalculatorGraphViewController.Favorites"

@interface GraphViewController() <GraphViewDataSource, CalculatorProgramsTableViewControllerDelegate>
    @property (weak, nonatomic) IBOutlet GraphView *graphView;
    @property (nonatomic) CGPoint origin;
    @property (nonatomic, weak) IBOutlet UIToolbar *toolbar;
@end


@implementation GraphViewController
@synthesize dotModeSelected = _dotModeSelected;
@synthesize graphView = _graphView;
@synthesize program = _program;
@synthesize origin = _origin;
@synthesize toolbar = _toolbar;
@synthesize expressionLabel = _expressionLabel;

- (void)setDotModeSelected:(BOOL)dotModeSelected
{
    _dotModeSelected = dotModeSelected;
    self.graphView.dotModeSelected = dotModeSelected;
    [self.graphView setNeedsDisplay];
}

- (void)setProgram:(id)program
{
    _program = program;
    [self.graphView setNeedsDisplay];
}

- (void)awakeFromNib
{ 
    [super awakeFromNib];
    self.splitViewController.delegate = self;
}

- (IBAction)addToFavorites:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *favorites = [[defaults objectForKey:FAVORITES_KEY] mutableCopy];
    
    if(!favorites) favorites = [NSMutableArray array];
    [favorites addObject:self.program];
    [defaults setObject:favorites forKey:FAVORITES_KEY];
    [defaults synchronize];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"Show Favorite Graphs"])
    {
        NSArray *programs = [[NSUserDefaults standardUserDefaults] objectForKey:FAVORITES_KEY];
        [segue.destinationViewController setPrograms:programs];
        [segue.destinationViewController setDelegate:self];
    }
}


- (BOOL) splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation
{
    return  UIInterfaceOrientationIsPortrait(orientation);
}

- (void)splitViewController:(UISplitViewController *)svc willHideViewController:(UIViewController *)aViewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)pc
{
    [pc setPopoverContentSize:CGSizeMake(320, 480)];
    barButtonItem.title = @"Calculator";
    
    NSMutableArray *toolbarItems = [self.toolbar.items mutableCopy];
    [toolbarItems insertObject:barButtonItem atIndex:0];
    self.toolbar.items = toolbarItems;
}

- (void)splitViewController:(UISplitViewController *)svc willShowViewController:(UIViewController *)aViewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    NSMutableArray *toolbarItems = [self.toolbar.items mutableCopy];
    [toolbarItems removeObject:barButtonItem];
    self.toolbar.items = toolbarItems;
}



- (NSArray *) plotsForGraphView:(GraphView *)sender
{
    NSMutableArray* plots = [[NSMutableArray alloc]init];
    
    // Adjust for scale/origin
    //NSLog(@"origin x: %g origin y: %g", sender.origin.x, sender.origin.y);
    //NSLog(@"scale: %g", sender.scale);
    
    for (float px = 0; px < sender.bounds.size.width; px++)
    {
        CGFloat x = (px / sender.scale) - (sender.origin.x / sender.scale);
                
        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithDouble:x] forKey:@"x"];
        
        id result = [CalculatorBrain runProgram:self.program usingVariableValues:dictionary];
        
        if([result isKindOfClass:[NSNumber class]])
        {
            float y = [result doubleValue];
            //NSLog(@"%g, %g",x, y);
        
            CGFloat py = sender.origin.y - (y * sender.scale);
            
            //NSLog(@"%g, %g", px, py);
            
            CGPoint viewPoint = CGPointMake(px, py);
            
            //CGPoint viewPoint = [self convertPoint:rawPoint forView:sender];
            
            [plots addObject:[NSValue valueWithCGPoint:viewPoint]]; 
        }
    }
    return plots;
}

- (void)setOrigin:(CGPoint)origin
{
    if(origin.x != _origin.x && origin.y != _origin.y)
    {
        _origin = origin;
        [self.graphView setOrigin:origin];
    }
}

- (void)handleGraphMoveGesture:(UIPanGestureRecognizer *)gesture
{
    if((gesture.state == UIGestureRecognizerStateChanged) ||
       (gesture.state == UIGestureRecognizerStateEnded))
    {       
        CGPoint translation = [gesture translationInView:self.graphView];

        CGFloat x = self.origin.x + translation.x;
        CGFloat y = self.origin.y + translation.y;
        
        self.origin = CGPointMake(x, y);
        [gesture setTranslation:CGPointZero inView:self.graphView];
    }
}

- (void)handleOriginMoveGesture:(UITapGestureRecognizer *)gesture
{
    if(gesture.state == UIGestureRecognizerStateEnded)
    {       
        CGPoint touchLocation = [gesture locationInView:self.graphView];
        
        self.origin = touchLocation;
    }
}


-(void)saveToUserDefaults:(NSDictionary*)prefs
{
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
	if (standardUserDefaults) {
		[standardUserDefaults setObject:prefs forKey:@"Prefs"];
		[standardUserDefaults synchronize];
	}
}

-(NSDictionary*)retrieveFromUserDefaults
{
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	NSDictionary *val = nil;
	
	if (standardUserDefaults) 
		val = [standardUserDefaults objectForKey:@"Prefs"];
	
	return val;
}

- (void)setGraphView:(GraphView *)graphView
{
    _graphView = graphView;
    [self.graphView addGestureRecognizer:[[UIPinchGestureRecognizer alloc] initWithTarget:self.graphView action:@selector(pinch:)]];
    [self.graphView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleGraphMoveGesture:)]];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleOriginMoveGesture:)];
    tap.numberOfTapsRequired = 3;
    
    [self.graphView addGestureRecognizer:tap];
    
    self.graphView.dataSource = self;
    
    NSDictionary *prefs = [self retrieveFromUserDefaults];
    
    if([prefs objectForKey:@"origin-x"] && [prefs objectForKey:@"origin-y"])
        self.origin = CGPointMake([[prefs objectForKey:@"origin-x"] doubleValue], [[prefs objectForKey:@"origin-y"] doubleValue]); 
    else
        self.origin =  CGPointMake(self.graphView.bounds.size.width/2, self.graphView.bounds.size.height/2);

    if([prefs objectForKey:@"scale"])
        self.graphView.scale = [[prefs objectForKey:@"scale"] doubleValue];
    
    graphView.dotModeSelected = self.dotModeSelected;
}

-(void)CalculatorProgramsTableViewController:(CalculatorProgramsTableViewController *)sender choseProgram:(id)program
{
    self.program = program;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
     if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
     {
         return YES;
     }
     else
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    NSDictionary *prefs = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:self.graphView.origin.x], @"origin-x", [NSNumber numberWithDouble:self.graphView.origin.y], @"origin-y", [NSNumber numberWithDouble:self.graphView.scale], @"scale", nil];
    
    [self saveToUserDefaults:prefs];
}

- (void)viewDidUnload {
    [self setGraphView:nil];
    [self setExpressionLabel:nil];
    [super viewDidUnload];
}
@end
