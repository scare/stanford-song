//
//  CalculatorBrain.h
//  Calculator
//
//  Created by Daniel Hirschlein on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalculatorBrain : NSObject

- (void)clearMemory;
- (void)pushOperand:(double)operand;
- (void)pushVariable:(NSString *)var;
- (void)popOperand;
- (id)performOperation:(NSString *)operation;

// program is always guaranteed to be a Property List
@property (readonly) id program;

+ (NSSet *)variablesUsedInProgram:(id)program;
+ (id)runProgram:(id)program;
+ (id)runProgram:(id)program usingVariableValues:(NSDictionary *)variableValues;
+ (NSString *)descriptionOfProgram:(id)program;


@end
